﻿BYLAWS OF
*Good Good Work, Coop*
(hereinafter the “Cooperative”)


# Preamble

**Whereas, the mission of the Cooperative is to support good groups in doing good work.**

Whereas, the Cooperative subscribes and commits to the following core values:

1. Work for something because it is good, not just because it stands a chance to succeed;
2. Equally value labor in all of its forms;
3. Uphold the ideals of:
 - authenticity
 - solidarity
 - prefiguration
 - transparency
4. Aim to achieve, in all of our work:
 - partnership
 - liberation
 - empowerment
 - efficacy
5. Do excellent work; and
6. Support good people doing good work

Whereas, the Cooperative adopts and subscribes to the seven International Cooperative Alliance Cooperative principles:

1. Voluntary and Open Membership.
2. Democratic Member Control.
3. Members' Economic Participation.
4. Autonomy and Independence.
5. Education, Training and Information.
6. Cooperation among Cooperatives.
7. Concern for Community.

Whereas, the Articles of Incorporation, as amended from time to time, are hereby incorporated by reference into these Bylaws.

# Article I
## Membership; Authorized Capital

### Section 1: Qualifications for Membership

Each of the following shall be a “Member” of the Cooperative.
**(a) Common Stock** - Member (also referred to as “Member”). Any natural person or limited liability entity may apply for admission to the Cooperative as a Member, who meets the following eligibility requirements and agrees to:

- Purchase one (`1`) Share of Common Stock in the Cooperative for a price per Share of $3000.00 (the “Common Share”), which price may be changed or increased from time to time by the Board;
- Acquire and at all times hold one and only one Common Share per person;
- Remain an active member in the Cooperative for a minimum of two (`2`) years;
- Patronize the Cooperative pursuant to a Membership Agreement and such other policies and agreements as required by the Board;
- At all times maintain good standing as a member of one or more classes of membership in the Cooperative;
- Participate in Cooperative governance functions and responsibilities;
- Execute such instruments and agreements as may reasonably be necessary or advisable for the Cooperative to carry out its lawful purpose(s) if authorized by the Board;
- Meet such other uniform conditions and qualification requirements as may be prescribed from time to time by the Board; and
- At all times abide by these Bylaws, and the rules and policies as may be established and adopted from time to time by the Members or the Board.
- Unless waived or modified by the Board in its sole discretion, shall have worked for the Cooperative on a part- or full-time basis as a “Contributor” (as the term is defined in the Cooperative’s policies and agreements, and pursuant to Work Agreement for no less than one (`1`) year.

**(b) Admission to Membership.** The Cooperative, by a passing vote of the Members present and entitled to vote, may admit to membership any applicant who:

1. applies for admission for the purpose of participating in the activities of the Cooperative;
2. meets all the requirements for application and membership under these Bylaws, the statutes of the State of Colorado and policies established by the Board;
3. purchases one (`1`) Share of Common Stock in the Cooperative;
4. executes a Membership Agreement or such other agreement as the Board may require; and
5. executes an agreement governing the work that Member is to perform; except that a person shall not be eligible for membership if the Board finds, based on reasonable grounds, which shall not include discrimination on the basis of sex, race, ethnicity, national origin, sexual orientation, gender, physical ability, medical history, religion or any other status protected by federal or state law, that the applicant's admission would prejudice the interests, hinder or otherwise obstruct, or conflict with, any purpose or operation of the Cooperative.

Without limiting the generality of the foregoing, the Cooperative may limit, postpone, delay or deny admission to an applicant for membership if the Members determine in their discretion that such admission would frustrate, jeopardize or in any other way adversely affect the optimal size of the Cooperative’s membership, which decision can be vetoed by the Board for cause. An applicant shall be considered a member effective upon acceptance of said human's or entity’s application, payment for the Membership Share in immediately available funds, and full execution of such agreements as the Board may require. Without limiting the generality of the foregoing, Members may pay for the Common Share either in immediately available funds, via an installment payment plan, or via periodic deductions from payments or distributions made by the Cooperative to such Member. A new member's allocation of the Cooperative's Net Margins for the year in which said human became a member shall be based on the relationship of the member's patronage of the Cooperative after said human became a member to the total patronage of all members for that year.

**(c) Certificates of Interests in the Cooperative.** The Cooperative shall not be required to issue any certificates representing Shares or other investments in the Cooperative. If certificates are issued, the restrictions on transfer of Interest or membership shall be printed upon every certificate of Interest or certificate of membership subject to the restrictions. Certificates shall also include the terms and conditions of redemption, if any.

### Section 2: Restrictions on Transfer of Share

No Share may be transferred to any person or entity not otherwise qualified to be a Member in the Cooperative or that does not patronize the Cooperative, in accordance with Section 1 above, except: to a spouse for holding in co-tenancy or joint tenancy with a right of survivorship; to a business entity controlled by such holder; or to the Cooperative upon the redemption or acquisition thereof by the Cooperative. Any purported transfer or any transfer that results from the operation of law shall be void and of no effect, unless consented to in writing by the Board and entered into the records of the Cooperative. If in the sole discretion of the Board, membership is at any time held by any person or entity not otherwise eligible to hold the same, the Board may in its sole discretion, either redeem the proceeds of such Share, including any unredeemed notices of allocation, or transfer such Share to a non-membership equity account upon written notification to the holder thereof and the person or entity shall not be entitled to vote at the membership meeting of the Cooperative.

### Section 3

**(a) Member Withdrawal.** Notwithstanding that the terms, conditions and continuation of certain obligations pursuant to such agreements as may exist between a Member and the Cooperative shall continue in full force and effect notwithstanding the foregoing, and subject to each Member satisfying its minimum membership term as defined herein, a Member may withdraw from the Cooperative by providing sixty (`60`) calendar days written notice of the Member's intent to withdraw to the Secretary of the Cooperative or to such other representative as authorized by the Board. The form of such written withdrawal may be prescribed by the Board. A withdrawing Member shall be considered an active Member entitled to all benefits entitled and accruing thereto pursuant to these Bylaws until the withdrawal becomes effective. Unless a Member has withdrawn because the Member has died, dissolved its business, is no longer eligible for membership in the Cooperative, or because of a violation of any agreements, policies or procedures of the Cooperative, a Member who withdraws shall be eligible to reapply for membership in the Cooperative at any time following the effective date of such withdrawal. Notwithstanding a Member’s right to withdraw, the Board reserves the right to delay, postpone, withdraw, suspend or otherwise decide unilaterally the timing and method by which the equity represented by a Member’s Membership Share may be redeemed. The Board shall have the sole discretion to determine the timing and method of any redemption of a Member’s equity.

**(b) Termination of Members.** If, following a hearing, prior to which 10 calendar days written notice of intention to terminate was given to a Member by the Cooperative, the Board or such other authorized Committee shall find that the member has:

1. ceased patronizing the Cooperative, or has failed to meet its patronage obligations as provided in the Membership Agreement for a consecutive period of two (`2`) months;
2. has violated any other provision of the Membership Agreement or any other policy or procedures of the Cooperative;
3. died, dissolved its business, or has otherwise ceased patronage activities;
4. otherwise ceased to be eligible for membership in the Cooperative;
5. otherwise been disruptive to the orderly operation of the Cooperative or frustrated the Cooperative’s purpose or efforts, the Board may terminate, effective immediately, the Member’s voting rights and membership in the Cooperative.

**(c) Rights and Interest on Withdrawal or Termination.** On the date a Member's withdrawal becomes effective or upon the termination of the Member's membership in the Cooperative by the Board, all rights and interests of the Member in the Cooperative shall cease and the Member shall be entitled only to payment for the value of the Member's equity interest in the Cooperative, as defined in this Section 3. The equity interest of Members are defined as the balance of that Member’s Capital Account (as defined herein), connected to the purchase of each one (`1`) Share of Common Stock per membership class acquired as a condition for membership in each membership class of the Cooperative (“Terminated Membership Redemption Price”). Within one hundred and twenty (`120`) days after the effective date of the Member's withdrawal or termination, the Cooperative shall consider distributing to the Member the Terminated Membership Redemption Price, either in cash, by issuing a promissory note, or some combination thereof, to be decided in the sole discretion of the Board. Notwithstanding the foregoing, the Board shall have the sole discretion to delay, withhold, modify or otherwise control the timing of any redemption or equity distribution if it would impair the financial health of the Cooperative.

### Section 4: Representations of Certain Members

If a Member of the Cooperative is other than a natural person, the Member may be represented by any individual, associate, Officer, manager, or member thereof duly authorized by the Member in writing delivered to the Secretary of the Cooperative.

### Section 5: Consent to Tax Treatment

Each natural person or entity which hereafter becomes a Member in this Cooperative on or after January 1, 2017, and which continues as a Member after such date shall by such act alone consents that the amounts and method of any distributions in connection with the Member's patronage occurring on or after January 1, 2017, which are made in qualified written notices of allocation (as defined in 26 U.S.C. 1388) and which are received by the Member from the cooperative, will be taken into account by the Member at their stated dollar amounts in the manner provided in 26 U.S.C. 1385(a) in the taxable year in which the qualified written notices of allocation are received by the Member. Each Member shall be solely responsible for any tax liability incurred as a result of patronage with the Cooperative. Each Member shall indemnify and forever hold harmless the Cooperative from any claims whatsoever of any kind resulting from or arising out of patronage or its purchase or holding of common or preferred stock.

### Section 6: Record of Members

A record of the Members and their full names, addresses, and social security or tax identification numbers shall be kept by the Cooperative. Each Member shall notify the Secretary immediately of any change in the Member's address, social security number or tax identification number.

# Article II
## Meetings of Members

For all purposes under these bylaws, for so long as the number of Members is less than eight (`8`), the Members shall constitute the Initial Board and all meetings of and actions taken by the Members shall constitute a meeting of or action by, respectively, the Board, and vice-a-versa.

### Section 1: Meetings of Members

Meetings of Members of the Cooperative, as previously determined by the Board, may take place in person, by telephone conference, by Internet conference, by video conference, or by any other electronic or telecommunications means by which the Members can effectively communicate, following the notice procedures prescribed in these Bylaws.

### Section 2: Regular Annual Membership Meetings

Regular membership meetings of all Members shall be held annually on or about the Spring Equinox, the Summer Solstice, November 20 or on a date and at such time and place in the area served by the Cooperative as may be determined by the Board and specified in the proper notice of the meeting. Regular meetings of the Members shall be conducted monthly in a “Change Up” format. At all regular meetings of Members, any and all lawful business may be brought before the meeting regardless of whether stated in the notice of the meeting; except that amendments to the Articles of Incorporation or the Bylaws of the Cooperative or other action required to be stated in the notice of the meeting shall not be subject to action unless notice thereof is stated in the notice of the meeting. The Secretary shall prepare and post the membership list in a conspicuous location during a regular annual membership meeting.

1. The purpose of the Spring Equinox membership meeting is to report on and review the prior fiscal year’s financial performance.
2. The purpose of the Summer Solstice membership meeting is to review and discuss the meta structure of the Cooperative. This meeting shall include Members and may include collaborators, as determined by the Board.
3. The purpose of the November 20 membership meeting is to set intentions for the Cooperative for the upcoming year. This meeting shall include Members and may include collaborators, as determined by the Board.
4. The purpose of the monthly membership meeting is to set culture and protocol. Monthly membership meetings shall include Members and may include collaborators, as determined by the Board.

### Section 3: Special Membership Meetings

Special meetings of the Members of the Cooperative may be called at any time by order of the Board, by such Officer(s) as may be designated in these Bylaws, or upon a written petition of at least twenty-five percent (`25%`) of the Members, such petition delivered to the President or the Secretary of the Cooperative stating the specific business to be brought before the meeting and shall state the time, date and place of the meeting. The petition shall specify a date for such Special Membership Meeting that is no less than ten (`10`) days and no more than sixty (`60`) days from the date of the petition. The place stated in the petition shall be a place reasonably convenient for the general membership. At all special meetings of the members of the Cooperative, business brought before the meeting shall be limited to the purpose stated in the notice. The Secretary shall prepare shall post the Membership List in a conspicuous location during a special membership meeting.

### Section 4: Notice of Meetings

Written notice of every regular and special meeting of the Members shall be prepared and mailed or electronically mailed to the last known U.S. Post Office or email address of each Member not less than ten (10) days before the meeting. The notice shall state the time and place, the business to come before the meeting. No business shall be transacted at special meetings other than that referred to in the written notice.

### Section 5

**(a) Waiver of Notice.** When any notice is required to be given to any Member of the Cooperative by law or under the provisions of the Articles of Incorporation or these Bylaws, a waiver thereof shall be equivalent to the delivery of proper notice, provided such waiver is in writing signed by the Member entitled to the notice, whether before, at, or after the time stated in the notice, including an email.

**(b) Waiver by Attendance.** By attending a meeting, a Member:

1. waives objection to lack of notice or defective notice of the meeting unless the Member, at the beginning of the meeting, objects to the holding of the meeting or the transacting of business at the meeting; and
2. waives objection to consideration at the meeting of a particular matter not within the purpose or purposes described in the meeting notice unless the Member objects to considering the matter when it is presented. “Attendance”" shall include attendance in person at any meeting, participating in a telephonic meeting, or participation by signing into a tele-conference or other form of Internet on-line meeting format as prescribed by the Board for that particular meeting.

### Section 6

**(a) Voting at Meetings.** At all membership meetings, each qualified Member holding one (`1`) Share of voting Common Stock, provided such Member is in good standing in accordance with all policies duly adopted by the Board, shall be entitled to one (`1`) vote for each Share of voting Common Stock held (“Member Voting Class”). Provided quorum exists, all matters shall require an affirmative vote of a two-thirds super-majority of the Member Voting Class present and entitled to vote, except as otherwise specifically provided by law, the Articles of Incorporation or these Bylaws. Votes shall only be counted among Members present and entitled to vote, including proxy votes.

**(b) Proxy and Cumulative Voting.** Voting by proxy is permitted at all meetings, provided the proxy authorization is memorialized in writing, signed by both Members. Cumulative voting is prohibited at any and all meetings of the Cooperative. For purposes of this subsection (b), all duly prepared and delivered powers of attorney shall be considered to be proxies.

**(c) Voting by Mail or by Electronic Means.** For any meetings of Members, the Board, at its election, may submit motions, resolutions, or other matters to be voted upon to all Members for vote by ballots transmitted by mail through the U.S. Postal Service. In addition, the Board, at its election, may submit motions, resolutions, or other matters to be voted upon to all Members for vote by any electronic means (including, but not limited to, email ballots, Internet drop box voting, electronic voting systems, etc.) that the Board deems reasonable and that will allow all of the Members to vote. Email ballots shall be deemed properly delivered when transmitted by sender. The ballots may be returned to the Cooperative by mail, by email, or by any other reasonable means, as directed in instructions to be delivered with the ballots. The ballots shall be counted only in the meeting at the time at which the vote is taken, provided that all Members have been notified in writing, pursuant to action by the Board, of the exact wording of the motion or resolution upon which the vote is taken, and a copy of the motion or resolution is forwarded with and attached to the vote of the Member voting. If a matter, for which mailed or emailed ballots have been delivered and received by the Cooperative, has been amended at the meeting, the meeting shall be adjourned with respect to that matter until a new vote can be solicited by mail or email with respect to the amended matter. Notwithstanding the foregoing, if a quorum is present at such meeting and a simple majority vote of the Member Voting Class approves the matter as amended, the meeting need not be adjourned with respect to that amended matter.


### Section 7: Quorum

A simple majority of the Members, present and voting in person or in any other manner authorized by these Bylaws, shall constitute a quorum for the transaction of business at any meeting of the Members, except for the transaction of business concerning which a different quorum is specifically provided by law. In the event a quorum is not present or is lost during the meeting, the meeting may be recessed or adjourned from time to time without further notice by a majority of those present until a quorum is obtained. Any business may be transacted at the resumption of the recessed meeting that might have been transacted at the originally called meeting. In the event a quorum is not present or is lost during the meeting, the meeting may be recessed or adjourned from time to time without further notice by a majority of those present until a quorum is obtained. Any business may be transacted at the resumption of the recessed meeting that might have been transacted at the originally called meeting.


### Section 8: Order of Business

All membership meetings of the Cooperative shall be presided upon in accordance with these Bylaws. The Officer presiding over membership meetings shall have the discretion to adopt and enforce formal governance procedures and rules. The following order of business shall be used as a guide insofar as is applicable and desirable:

0. Check In
1. Determination of quorum
2. Proof of due notice of meeting
3. Reading and disposition of minutes
4. Treasurer’s report
5. Report of Board by president or vice president
6. Report of secretary-treasurer
7. Report of general manager
8. Reports of committees
9. Nominations for vacancies on the Board
10. Elections
11. Unfinished business
12. New business
13. Gratitudes
14. Adjournment


### Section 9: Action without a Meeting

Actions of the Members may be taken without a meeting if the action is agreed to by all Members eligible to vote on such matter, and approval to take such action is evidenced by one or more written consents or electronically transmitted approvals, signed by all Members entitled to vote on such matter and filed with the corporate records reflecting the action taken.

### Section 10: Matters Requiring Member Approval

In addition to those matters for which Member approval is required as a matter of custom or law, the following matters are so integral to the ethos and operations of the Cooperative that they shall require the approval of the Members:

1. Adoption of ethics code and amendments or changes thereto;
2. Election of Board; and
3. Board compensation plan.
4. Management structure.
5. Matters as determined by the Board or by petition of the members.

### Section 11: Required Attendance and Participation

Each Member shall attend and participate in at least 75% of membership meetings, matters presented for electronic voting or actions proposed to be taken without a meeting, as defined in this Article (collectively the “Membership Activities”). If a Member fails to attend at least 75% of Membership Activities within the prior one year, the Board may, but shall not be required to, suspend that Member’s economic and/or voting privileges or suspend that Member’s membership in the Cooperative.

# Article III
## Directors

### Section 1: Number and Qualifications of Directors

**(a)** The initial Board of Directors shall consist of the Members, and who shall each serve until the sooner of (i) such time as the number of Council Members exceeds seven (`7`); or (ii) annual meeting of the Members in 2018 ("Initial Board").

**(b)** Thereafter, the Board of Directors shall consist of no fewer than three (`3`) and up to a maximum of seven (`7`) natural persons, who are each at least eighteen (`18`) years of age. The Board may include up to twenty percent (`20%`) of the Board seats who are neither Members nor representatives of Members. Directors who are not Members or Member representatives may be elected by a vote of two-thirds of the Members present and voting. All directors shall be elected by the Members, in accordance with these bylaws (the “Board”).

**(c)** Persons from the Initial Board who are Members shall be entitled to serve an initial term of one (`1`) year, and each shall be eligible for re-election to serve on succeeding boards, on such terms as are prescribed herein. A vacancy on the Board may be declared at the discretion of the Board after any Director fails to attend three (`3`) consecutive regular Board meetings without cause and a replacement Director may be appointed as provided in Section 7 of this Article.

### Section 2

**(a) Nomination of Directors.** In years in which a director election is to occur, at the first meeting of the Board following the end of each fiscal year but not later than sixty (`60`) days preceding the annual meeting date, the Board shall strive to name at least two (`2`) nominees for each available Board Seat (as defined below), each qualified for a respective Board seat as prescribed herein. Each nominee shall have agreed to accept the directorship and its responsibilities if elected. The Board shall use the foregoing qualifications under the Directors' qualifications section of these Bylaws and shall nominate persons representative of a respective membership class. Each nominee must be willing to accept all the responsibilities of Directors of the Cooperative, to attend the Directors' meetings and other training and informational meetings to better serve as Directors and to become familiar with the Cooperative's Articles of Incorporation, Bylaws, organizational structure, objectives, policies and procedures.

**(b) Election of Directors.** Each Board Seat (collectively the “Board Seats”) shall be filled separately and election shall be as prescribed by the Board in person, or by mail or email ballots. Newly elected Directors shall become members of the Board at the first meeting of the Board following their election. To be elected, a nominee for a Board Seat shall either (i) be one of the candidates receiving the highest number of votes of all Members present and entitled to vote to prevail in a contested election; or (ii) receive a two-thirds super-majority vote of all Members present and entitled to vote in an un-contested election.

### Section 3: Term

Directors shall be elected for a term of two (`2`) years, except that the terms of Directors shall be staggered so that the terms of no more than a minority of then-existing director seats shall expire in any one year and the initial term of a Director elected to fill a vacancy shall be only for the remaining period of the unexpired term.

### Section 4: Election of Officers

The Board shall hold a meeting within thirty (`30`) days after the adjournment of the annual membership meeting for the purpose of organizing the Board. Nominations for the election of Officers shall be made by Directors from the floor at the Director's meeting where the Officers are to be elected. They shall elect a President. The Board may elect one or more Vice Presidents, a Secretary and a Treasurer as determined in the discretion of the Board. Each Officer shall hold office until the election and qualification of a successor unless earlier removed by death, resignation, or in accordance of these Bylaws. The Board may create, alter, and abolish such additional offices and its attendant duties in its discretion and may appoint persons to serve in such offices at the pleasure of the Board.

### Section 5: Removal of Officers or Directors

Provided the party initiating removal of a Director or Officer, as the case may be, has referred the charge for which removal is sought to the Culture Committee and attempted to resolve the matter in good faith in accordance with Article X of these bylaws,
**(a)** At a meeting called expressly for the purpose of considering removal, as well as any other proper purpose, a Director may be removed by the Members in the manner provided in this Section. Removal of a Director requires an affirmative vote of a two-thirds (`67%`) super-majority of Members present and voting if in person, by mail or by email. If removal of a Director is by the Board, then by a two-thirds (`67%`) super-majority of the members of the Board not subject to removal.

**(b)** The Board may remove a Director who does not meet the qualifications for Board membership set forth in these Bylaws.

**(c)** Members may remove one or more Directors with or without cause. A written petition signed by at least a twenty-five percent (`25%`) of Members may initiate a vote to remove a Director, in accordance with Section 5(a) above. No petition shall seek removal of more than one (`1`) Director.

**(d)** The Board shall have the power to remove any Officer of the Cooperative with or without cause, by a simple majority vote of the Directors not serving as the Officer subject to removal.

### Section 6: Referendum

Upon demand of at least one half (`50%`) of the entire Board, made immediately at the same meeting at which the original motion was passed and so recorded, any matter of policy that has been approved or passed by the Board must be referred to the Membership for ratification at the next regular or special meeting of the members, and a special meeting may be called for that purpose.

### Section 7: Vacancies

Whenever a vacancy occurs in the Board, except from the expiration of a term of office, the remaining Directors shall, as soon as practicable, appoint a Member from the same respective membership class as that from which the vacancy arose to fill the vacancy until the expiration of the term of the vacant position.

### Section 8: Board Meetings

Regular meetings shall be held by the Board at least once per fiscal year or more frequently, at such place (including online) and time as the Board may determine.

### Section 9: Special Meetings

Special meetings of the Board shall be held whenever called by the President or by a majority of Directors at a time and place specified in the notice (including online meetings). Any and all business may be transacted at any special meeting. A meeting of the Board may be held at any time or place with or without notice upon the consent of all the Directors.

### Section 10: Notice of Board Meetings

Prior written notice of each meeting of the Board shall be delivered to each Director at least ten (`10`) calendar days for regular meetings and at least three (`3`) business days for any special meetings, provided, that the Board may establish regular meeting places, dates and times for which the aforementioned notice need not be given. Notice may be waived by any or all of the Directors, and appearance at a meeting shall constitute a waiver of notice thereof, except if a Director attends a meeting for the express purpose of objecting to the transaction of any business on the ground that the meeting has not been lawfully called or convened.

### Section 11: Telephonic Meeting

One or more members of the Board or any committee designated by the Board may participate in a meeting of the Board or committee by means of conference telephone or similar communications medium by which all persons participating in the meeting can communicate effectively. Such participation shall constitute presence in person at the meeting.

### Section 12: Quorum; Voting

A simple majority of the Board shall constitute a quorum at any meeting of the Board. In the event a quorum is lost during a meeting, however, the meeting may proceed. Each member of the Board, including each Officer who is a member of the Board, shall be entitled to one (`1`) vote per member of the Board on any matter coming before the Board, except, no Director shall vote on any matter in which said human has a pecuniary self-interest in any capacity other than as a Member of the Cooperative. A Director who has a pecuniary self-interest may, however, vote on such a matter if the remaining disinterested Directors ratify the vote on such matter and deem the decision to be in the best interest of the Cooperative. Any matter upon which the Board may vote shall require a two-thirds (`67%`) super-majority affirmative vote of those present and voting to pass.

### Section 13: Assent to Action

A Director is considered to have assented to an action of the Board unless:

1. The Director votes against it or abstains and causes the abstention to be recorded in the minutes of the meeting;
2. The Director objects at the beginning of the meeting and does not later vote for it;
3. The Director has their dissent recorded in the minutes;
4. The Director does not attend the meeting at which the vote is taken; or
5. The Director gives notice of their objection in writing to the Secretary within twenty-four (`24`) hours after the meeting.

### Section 14: Action without a Meeting

Actions of the Board may be taken without a meeting if the action is agreed to by all Directors and is evidenced by one or more written consents signed, or electronically submitted via email, by all Directors and filed with the corporate records reflecting the action taken.

### Section 15: Compensation

Reasonable procedures for the expense reimbursement of the members of the Board shall be established by the Board. Directors may be eligible for reasonable compensation for their service as directors. Notwithstanding the foregoing, directors may be eligible for compensation arising from or as provided for in a Membership Agreement, Employment Agreement or any other agreement governing the terms and conditions of a Share in the Cooperative. At the first regular Board meeting of each fiscal year the reimbursement policies shall be established. Directors may be reimbursed for actual and reasonable out of pocket expenses incurred in service to the Cooperative.

### Section 16: Other Committees

The Board may, in its discretion, appoint such other committees from its own number or from the membership, as may be necessary.

### Section 17: General Standards of Conduct for Directors and Officers

**(a)** Each Director shall discharge their duties as a Director, including duties as a member of a committee, and each Officer with discretionary authority shall discharge their duties under that authority:

1. In good faith and proper purpose;
2. With the care an ordinary prudent person in a like position would exercise under similar circumstances;
3. In a manner the Director reasonably believes to be in the best interests of the Cooperative and its Membership; and
4. And in accordance with Article VII of the Articles of Incorporation (as may be amended and restated).

**(b)** In discharging said human's duties, a Director or Officer is entitled to rely on information, opinions, reports, or statements, including financial statements and other financial data, if prepared or presented by:

1. one or more Officers or employees of the Cooperative whom the Director or Officer reasonably believes to be reliable and competent in the matters presented;
2. legal counsel, a public accountant, or another person as to matters the Director or Officer reasonably believes are within such person's professional or expert competence; or
3. in the case of a Director, a committee of the Board of which the Director is to a member if the Director reasonably believes the committee merits confidence.

**(c)** A Director or Officer is not acting in good faith if said human has knowledge concerning the matter in question that makes reliance otherwise permitted by subsection (b) of this section unwarranted.

### Section 18: Indemnification

In accordance with Article VIII of the Articles of Incorporation, the Cooperative shall indemnify, to the fullest extent permitted by applicable law in effect from time to time, any person, and the estate and personal representative of any such person, against all liability and expense (including attorney’s fees) incurred by reason of the fact that said human is or was a Director or Officer of the Cooperative or, while serving as a Director or Officer of the Cooperative, said human is or was serving at the request of the Cooperative as a Director, Officer, partner, trustee, employee, fiduciary, or agent of, or in any similar managerial or fiduciary position of, another domestic or foreign Cooperative or other individual or entity or of an employee benefit plan. The Cooperative shall also indemnify any person who is serving or has served the Cooperative as Director, Officer, employee, fiduciary or agent, and the estate and personal representative of any such person, to the extent and in the manner provided in any bylaw, resolution of the Board or the shareholders, contract, or otherwise, so long as such provision is legally permissible. Notwithstanding anything to the contrary set forth in this Article VIII (Articles of Incorporation), such indemnity shall not extend to conduct not undertaken in good faith to promote the best interests of the Cooperative, nor to any recklessness or willful misconduct; and, provided further, that this indemnification shall be limited to the total assets of the Cooperative

# Article IV
## Duties of Directors

### Section 1: Management of Business

The Board shall have general supervision and control of the business and the affairs of the Cooperative and shall make all rules and regulations not inconsistent with law, the Articles of Incorporation or with these Bylaws for the management of the business and the guidance of the Members, Officers, employees, and agents of the Cooperative. The Board shall have installed an accounting system which shall be adequate to the requirements of the business, and it shall be the duty of the Directors to require proper records to be kept of all business transactions.

### Section 2: Reports of Business Activity and Finances

The Board shall present at each regular meeting of the Members and, if appropriate, at special meetings of the Members a detailed statement or report of the business of the preceding year. The statements shall show the financial condition of the Cooperative at the end of the fiscal year and shall be in a form as shall fully exhibit to the Members a complete illustration of the assets and liabilities of the Cooperative, of the cash on hand, inventory, and indebtedness and all other facts and figures pertinent to a complete understanding of the Cooperative's financial position for the period.

### Section 3: Review of Financials

**(a) Annual Review.** The Board shall have a comprehensive review of the Cooperative’s financial statements made at least at the end of each fiscal year and at other times as it deems necessary. This comprehensive review shall meet these requirements:

# [REQUIREMENTS MISSING HERE]

**(b) Performance.** The review may be performed by the Treasurer or the Cooperative’s accountant hired by the Directors of the Cooperative.

**(c) Scope.** The examination is to be made in accordance with generally accepted auditing and accounting standards and the reviewer is to express an independent opinion as to the fairness of the basic financial statements taken as a whole or clearly state why an unqualified opinion cannot be rendered. The review report shall contain no significant qualifications caused by limitations on the scope of the examination.

**(d) Form.** The review report shall be in written or electronic form and shall be presented to the Board and reviewed with the Directors at a regular or special meeting as determined by the Directors and the auditor, following the completion of the review. The review report shall be reviewed with the Members of the Cooperative at the annual meeting. Copies of the completed review report shall be presented to each of the Directors. The records of the Cooperative shall be available at the Cooperative for the Treasurer to review at any time during the year. Each Member shall be given, each year, a summary financial statement based on the annual review report which statement shall indicate that a copy of the annual review report is available at the office of the Cooperative for review by any Members.

### Section 4: Depository

The Board shall have the power to select one or more banks to act as depositories of the funds of the Cooperative and to determine the manner of receiving, depositing, and disbursing the funds of the Cooperative and the form of checks – or other types of electronic payments – and the person or persons by whom checks shall be signed, with the power to change banks and the person or persons signing checks and the form thereof at will.

### Section 5: Agreements with Members

The Board shall have the power to carry out all agreements of the Cooperative with its Members in every way advantageous to the Cooperative representing the Members collectively.

### Section 6: Nepotism

No immediate relative of any Director shall be regularly employed by the Cooperative unless approved in writing by a vote of a two-thirds super-majority of disinterested Directors. Immediate relative is defined as father, mother, brother, sister, spouse, common law domestic partner, son, daughter, son-in-law, or daughter-in-law.

# Article V
## Duties of Board Officers

### Section 1: Duties of the President

The President shall:

1. preside over all meetings of the Cooperative and of the Board,
2. call special meetings of the Board,
3. perform all acts and duties usually performed by a presiding Officer,
4. sign such instruments of the Cooperative as said human may be authorized or directed to sign by the Board; provided, however, that the Board may authorize any person to sign any or all checks, contracts and other instruments in writing in behalf of the Cooperative; and
5. the President shall perform such other duties as may be prescribed by the Board.

### Section 2: Duties of the Vice Chair

If so elected by the Board pursuant to Article III, Section 4 hereof, in the absence or disability of the President, the Vice Chair shall perform the duties of the President. The Vice Chair shall perform such other duties as may be required by the Board. If such Officer has not been elected by the Board, then the foregoing duties shall be the responsibility of the President.

### Section 3: Duties of the Secretary

If so elected by the Board pursuant to Article III, Section 4 hereof, the Secretary shall keep a complete record of all meetings of the Cooperative and of the Board and shall have general charge and supervision of the corporate records of the Cooperative. Said human shall serve all notices required by law and by these Bylaws and shall make a full report of all matters and business pertaining to the office and to the Members at the annual meeting. The copies of the Board or membership minutes, and complete membership records shall be maintained at the principal office of the Cooperative. The Secretary shall make corporate reports required by law and shall perform such other duties as may be required of the position by the Cooperative or by the Board. If such Officer has not been elected by the Board, then the foregoing duties shall be the responsibility of the President.

### Section 4: Duties of the Treasurer

If so elected by the Board pursuant to Article III, Section 4 hereof, the Treasurer shall have supervision of the Cooperative's financial records and perform such duties with respect to the finances of the Cooperative as may be prescribed by the Board. Upon the election of their successor, the Treasurer shall turn over all books and other property belonging to the Cooperative in their possession. If such Officer has not been elected by the Board, then the foregoing duties shall be the responsibility of the President.

# Article VI
## Management

### Section 1: Duties in General

Under the direction of the Board, if so employed, the CEO shall have general charge of the ordinary and usual business operations of the Cooperative. The CEO shall, so far as practicable, endeavor to conduct the business in such a manner that the Members will receive just and fair treatment. The CEO shall cause all money belonging to the Cooperative to be deposited in a bank or invested in a manner selected by the Board and if authorized to do so by the Board shall make all disbursements by check or withdrawal therefrom for the ordinary and necessary expenses of the business in the manner and form prescribed by the Board. Upon the appointment of their successor, the CEO shall deliver all money and property belonging to the Cooperative which in their possession or control.

### Section 2: Duties of CEO to Account

The CEO shall be required to maintain Cooperative records and accounts in such a manner that the true and correct condition of the business may be ascertained therefrom at any time. Monthly and annual statements shall be prepared in the form and in the manner prescribed by the Board. All books, documents, correspondence, and records of whatever kind pertaining to the business which may come into their possession shall be carefully preserved.

### Section 3: Duties of CEO Concerning Employees

The CEO shall have the authority to employ, supervise, and terminate all employees of the Cooperative and fix their compensation subject to the policies and at salaries within ranges adopted by the Board not inconsistent with these Bylaws. Employees shall be under the direct supervision of the CEO. Auditors, agents, or counsel specifically employed by the Board shall be under the supervision of the Board and not under the CEO.

# Article VII
## Capital

### Section 1

**(a) Investments in Equity Capital.** The Board may require that Members make additional or supplemental capital contributions to the Cooperative on a percentage or other basis established in a written policy of the Board furnished to each Member or in any applicable membership or other agreement.

**(b) Notice of Records.** All allocated shares of Net Margins shall be deemed capital contributions in the Cooperative without any further action by the Cooperative other than the giving to the appropriate recipient a written notice of allocation (as defined in 26 U.S.C. 1388). The Cooperative shall keep appropriate books and records showing the capital contribution by each Member in each year. The Cooperative may, but shall not be required to, issue such additional evidence of capital contribution in the Cooperative as the Board may prescribe.

### Section 2

**(a) Computation of Net Margins.** The Cooperative's Net Margins, calculated upon the basis of each fiscal year, shall be computed as follows:

1. Gross Receipts. All proceeds of all sales or other revenue resulting from the ordinary course of the Cooperative’s operating activities, plus all sums received from all other sources except loans and contributions to this Cooperative and investments in its capital, shall be deemed to be "Gross Receipts."
2. Deductions from Gross Receipts. This Cooperative shall deduct from the Gross Receipts the sum of the following items:
 - All costs and expenses and other charges which are lawfully excludable or deductible from this Cooperative's Gross Receipts for the purpose of determining the amount of any Net Margins of this Cooperative.
 - Reserves. The Board may establish amounts for reasonable and necessary reserves for bad debts, contingent losses, working capital, debt retirement, and membership equity retirement. Unless allocated among the Members entitled to share in allocations of the Cooperative's Net Margins,
   - (a) the Cooperative shall include the amounts credited to the reserves in computing its taxable income,
   - (b) the tax liability thereon shall be deducted from Net Margins, and
   - (c) no Member or other person entitled to share in the allocation of the Cooperative's Net Margins shall have any right or interest at any time in or to the reserve funds of the Cooperative except upon dissolution when the entire reserve funds of the Cooperative shall be distributed in accordance with the law and these Bylaws.
 - Contributions to Surplus. The Net Margins, less any tax liability of the Cooperative accruing therefrom, attributable to business done for persons who are not Members or otherwise qualified to share in allocations of Net Margins or otherwise derived from non-patronage related sources (“Non-Member Patronage”) may be retained as property of the Cooperative in a surplus fund to be used as additional working capital or for such other purposes as may be determined by the Board of Directors. This surplus fund shall be distributed only upon dissolution of the Cooperative and no Member shall at any time have any right or interest in or to the surplus fund, except on dissolution. The Cooperative may conduct business with non-members such that Non-Member Patronage never exceeds forty-nine (49%) percent of the total patronage (member plus non-member patronage) with the Cooperative.

**(b) Cooperative's Net Margins.** The balance of said Gross Receipts which remains after the foregoing deductions shall be deemed to be the "Cooperative's Net Margins" (or “Net Margin”) which term shall encompass Net Margins of Members entitled to share in the allocation of Net Margins of the Cooperative.

**(c) Losses.** In the event the Cooperative sustains a loss in any manner for any period resulting from, among other things, operations, casualty, revaluation of assets or otherwise with respect to the Cooperative as a whole or from a particular segment of the Cooperative's operations, the Board shall determine the manner in which the loss shall be taken into account for accounting, taxation or any other purposes; provided that in making its determination the Board shall take into account all applicable facts and circumstances and account for the loss on a basis which is fair and equitable to all Members in the Cooperative. In making its determination the Board may authorize actions including, but not limited to:

1. allocating the loss on an equitable basis to some or all of the Members of the Cooperative by debiting equity account balances, by charging Members directly, or by charging Members using non-qualified notices of allocation;
2. carrying the loss back or forward to offset earnings of the Cooperative or particular segments of its operations in prior or future years;
3. canceling or debiting any or all outstanding equity account balances shown on the books of the Cooperative; or
4. charging the loss against appropriate reserve or surplus accounts.

The Board may, but shall not be required to, submit a recommendation as to apportionment and allocation of any loss to a vote of the Members at a meeting of the Members duly called and properly held. A vote of a simple-majority of the Members present or voting by mail or by email at such a meeting shall be binding upon all the Members entitled to share in allocations of the Cooperative's Net Margins. To the maximum extent provided by law, no Member shall be liable for the debts of the Cooperative in an amount exceeding said human's Membership Share and any equity capital invested in the Cooperative.

**(d) Patronage Dividends.** The total Cooperative's Net Margins shall be received by the Cooperative, belong to and be held by the Cooperative for all its Members qualified to share in allocations of the Cooperative's Net Margins and shall be allocated to such Members at the close of each fiscal year on a patronage basis. Each Member’s respective allocated share of the Net Margins may be computed as determined by the Board of Directors upon the basis of said human's respective patronage of, and the Net Margins resulting from, the operations, the various departments, or segments of operations of this Cooperative and shall be in proportion to the quantity or value of the services provided to such Member (as hereafter prescribed). When making allocations through qualified written notices of allocation, this Cooperative shall within eight and one-half (`8.5`) months after the close of its fiscal year notify each Member in the form of a qualified written notice of allocation (as defined in 26 U.S.C. 1388) of said human's total allocation of Cooperative's Net Margins including the cash portion as well as the amount credited to said human's capital account. Each recipient shall treat said human's total allocation in the manner prescribed by Article I, Section 5, of these Bylaws and any applicable tax laws, regulations, and private letter rulings.

    “Patronage Activity” shall mean the aggregate value of the Cooperative’s goods and services contributed or purchased or contributed by each member during the applicable fiscal period. The Board shall have the authority to develop, review and revise the methodology by which to calculate the Cooperative’s aggregate Patronage Activity and each member’s respective allocable Unit of Patronage Activity. Each member’s allocable Unit of the Cooperative’s Net Margins and Net Losses shall be made according to each member’s relative share of the aggregate Patronage Activity.

**(e) Qualified and Nonqualified Allocations.** Allocations of the Cooperative's Net Margins in accordance with this Article may be made in the form of qualified written notices of allocation or nonqualified written notices of allocation as determined by the Board.

**(f) Qualified Notice of Allocation, Payment and Reinvestment.** If the Cooperative pays any portion of an allocation of the Cooperative's Net Margins by a qualified written notice of allocation, the Board shall authorize at such time as it may determine, but in no event later than the fifteenth (`15th`) day of the ninth (`9th`) month following the end of the Cooperative's fiscal year, the Cooperative to pay in cash to each Member qualified to share in allocations of cooperative's Net Margins an amount as determined by the Board of at least twenty percent (`20%`) of the Member's allocated share of Net Margins and the balance of said human's allocated share of Net Margins shall be credited to the appropriate capital account of the Member on the books and records of the cooperative. The credit shall be deemed a payment to the Member and a reinvestment by the Member in the equity capital of the Cooperative.

### Section 3: Lien

To secure the payment of all indebtedness of any Member to this Cooperative, this Cooperative shall have perfected security interest and a first lien on the capital investments, Net Margins, and other property rights and interests, if any, in the Cooperative of such Member. As one means of enforcing its lien, the Cooperative shall be entitled to offset at any time, at the sole discretion of the Board, any debt of a Member person to the Cooperative with a corresponding amount of the Member's capital investments, Net Margins and other property rights and interests, if any, in the Cooperative. Each Member by joining and patronizing the Cooperative shall be deemed to have agreed to sign any instrument necessary to evidence and perfect the lien and security interest provided for in this Section.

### Section 4: No Offsets

No Member qualified to Share in allocations of Cooperative's Net Profits shall be entitled to demand offset of any portion of such person's allocated Share of Net Profit retained by the Cooperative against any indebtedness or claim due the Cooperative from such person.

### Section 5: Equity Redemption

**(a)** No acquisition, recall, distribution or redemption of equity capital in the Cooperative shall be made, required or effected, if the result of it would be to render the Cooperative unable to pay its debts as they become due in the usual course of business or causes the remaining assets of the Cooperative to be less than its liabilities plus the amount necessary to satisfy the interests of the holders of securities or other equity capital preferential to those receiving the distribution if the Cooperative were to be dissolved at the time of the distribution. Provided that the financial condition of the Cooperative will not be impaired, the Board in its sole discretion and subject to the approval of the Cooperative's secured creditors having the right to approve equity redemptions or retirements, and the application of the Cooperative Act, may, but shall not be obligated to, authorize the redemption of any equity capital in the Cooperative at any time when a Member owning equity capital in the Cooperative shall:

1. die,
2. if a non-natural person, liquidate its business affairs and intend to dissolve,
3. cease patronizing the Cooperative or using the Cooperative's services for a period of five (`5`) consecutive years,
4. withdraw or be terminated from the Cooperative as provided in these Bylaws, or
5. for other reasons as provided in an equity retirement policy adopted by the Board.

Each class of equity capital and all persons in each of the above classifications shall be treated similarly with their respective class or classification. The Board may, in its discretion, issue to the Member interest bearing certificates of indebtedness in substitution and exchange for the equity capital of a Member, which may be subject to redemption.

**(b)** When a Member separates from the Cooperative, whether through voluntary withdrawal, expulsion or death, the Cooperative shall redeem the Member's capital account pursuant to policies adopted by the Board, which policies may be revised from time to time in the sole discretion of the Board.

**(c)** No Member entitled to share in the allocation of the Cooperative's Net Margin shall have any right or interest at any time in or to any reserve fund, or surplus accounts, except upon dissolution of the Cooperative when any such reserve fund, or surplus account shall be distributed in accordance with these Bylaws, as otherwise provided by law or as the Directors may otherwise determine.

**(d)** In connection with or in addition to the foregoing, the Board may establish policies and practices for the redemption of equity capital based upon the recognition of difference in the character and liquidity of assets held by the Cooperative and the resulting impact on availability of funds for equity redemption.

### Section 6: Borrowed Capital

This Cooperative may borrow such additional capital from Members or any other person or source as permitted by law. It may issue notes or certificates of indebtedness for amounts of borrowed money with such terms and conditions and on which it may pay an interest rate as determined by the Board.

### Section 7: Commingling of Capital; No Interest

Investments in equity capital need not be segregated from, and may be invested in, or commingled with, any other assets of the Cooperative. Unless provided otherwise in these Bylaws, no Allocation, interest, or any other income shall be declared or paid on account of any capital Interest or other equity capital in the Cooperative owned by a member or other investor.

# Article VIII
## Dissolution; Liquidation; Cooperative Sale; Winding Up

Upon the dissolution, liquidation, sale of the Cooperative, or sale of all or substantially all of the Cooperative’s assets, all debts and liabilities of the Cooperative shall first be paid according to their respective priorities, as defined by law or by agreement. Any property or proceeds remaining after discharging the debts and liabilities of the Cooperative shall be distributed to the Members in the Cooperative's equity capital in accordance with the following priorities to the extent funds are available therefor, payments within each priority to be made on a pro-rata, pari passu basis without regard to time of investment:

1. First, to Members pro rata in accordance with each Member’s capital account balance; then
2. Finally, the remaining net proceeds (after deduction of (i) above) to Members pro rata in accordance with their respective Patronage Activity on the basis of each Member’s relative share of the total aggregate Patronage Activity of the Cooperative since its inception.

It is the desire of the initial Members that Members receiving liquidation distributions in connection with this Article VIII maintain an intention and strive to allocate fifty percent (`50%`) of each Member’s liquidation distribution to positive social and environmental causes consistent with the mission and purpose of this Cooperative, as described in the Preamble to these Bylaws.

If, in winding up of the affairs of the Cooperative, certain assets are not liquid, have no market value, creditors having claim on these assets have been satisfied and the trustees in liquidation or other persons charged with winding up the Cooperative's affairs have determined that the costs involved in delaying the winding up of the affairs of the Cooperative exceed the potential benefits, the trustees are authorized to assign the assets or any future proceeds from assets that are not liquid to a fund or account to be held in trust for the Members in accordance with this Article. The trustees shall under no circumstances be liable to any other Member or equity holder in the Cooperative for any claim on any assets assigned by the trustees pursuant to the authority of this Article.

# Article IX
## Unclaimed Money

A claim made against the Cooperative for money shall be subject to the provisions of this Article IX whenever the Cooperative is ready, able, and willing to pay the claim, and has paid or is paying generally claims arising under similar circumstances, but payment of the claim cannot be made for the reason that the Cooperative does not know the whereabouts or mailing address of the one to whom it is payable or the one entitled to payment. If the claim is not actually paid within a period of three (`3`) years after notification as herein provided, the Cooperative shall remove the claim as a liability on its books. No removal shall be made unless the Cooperative shall have sent by first class, United States mail, a written notice of eligibility for payment addressed to the person appearing on the Cooperative's records to be entitled to payment at the last address of such person shown by the records of the Cooperative. If not claimed within three (`3`) years after giving of notice, the claim shall be deemed extinguished. Any and all amounts recovered by the Cooperative pursuant to this Article IX, after deducting therefrom the amount of any taxes payable thereon, shall be placed in a reserve or surplus account established previously or hereafter by the Cooperative.

# Article X
## Dispute Resolution; Governing Law; Venue; Jurisdiction; Jury Trial Waiver

A Culture Committee shall be authorized and constituted by these Bylaws, which shall be comprised of up to eleven (`11`) members, each of whom possesses appropriate training and conflict resolution skills, and volunteers to be on call to serve on a panel to address conflict, cultural or normative tensions, or disputes between parties to these bylaws. The Board shall facilitate the creation of the Culture Committee and shall strive to recruit members from multiple like-minded organizations and shall strive for the Culture Committee to serve multiple participating organizations in the same capacity as herein. The Culture Committee shall seek to adhere to and enforce the Cooperative’s norms of conduct. The Board shall facilitate the creation of the Culture Committee and shall strive to recruit members from multiple like-minded organizations and shall strive for the Culture Committee to serve multiple participating organizations in the same capacity as herein. The Culture Committee shall seek to adhere to and enforce the Cooperative’s norms of conduct.

The party requesting the involvement of the Culture Committee shall initiate a conflict resolution process by stating its charge or reasons in writing. The written notice shall state the cause, reasons for the conflict and name the adverse party. If the requesting party seeks to initiate a conflict resolution process without an adverse party, but rather to address a systemic organizational or cultural tension, then the requesting party shall request a banc of three (`3`) Culture Committee members to hear and work on the matter. Members of the Conflict Committee shall mind conflicts of interest and recuse themselves when appropriate.

If the requesting party states a charge or describes a conflict with an adverse party, then each party shall be entitled to select two (`2`) members of the Culture Committee panel to serve on the Culture Committee instance, ranking each selection in order of preference. If no three (`3`) names match the selection of both parties, then each party’s first choice shall be selected and the and a third Culture Committee member shall be selected at random.

The primary role of the Culture Committee instance involved in resolving a cultural tension or inter-party conflict is to hold a mediation process to facilitate a healthy, voluntary resolution between the parties involved. If no mediation resolution can be reached, the Culture Committee instance is authorized to adjudicate the matter after taking all relevant facts and circumstances into consideration. The decision of the Culture Committee instance shall be in writing and shall be entitled to deference in terms of the facts. A party aggrieved by a Culture Committee decision may appeal the decision to the Board, whose decision is final and non-appealable.

These Bylaws shall be governed by and construed in accordance with the laws of the State of Colorado including all matters of construction, validity and performance. Members and the Cooperative agree that any action or proceeding commenced under or with respect to these Bylaws shall be brought only in the district courts of the County of Denver, State of Colorado, and the parties irrevocably consent to the jurisdiction of such courts and waive any right to alter or change venue, including by removal.

# Article XI
## Fiscal Year

The fiscal year of this Cooperative shall commence on January 1 each year and shall end on the following December 31.

# Article XII
## amendments

Amendments to these Bylaws shall be proposed and recommended by two-thirds (`67%`) of the Board, and thereafter ratified by a simple majority vote of Members. If upon presentment of a petition presented to the Secretary and signed by twenty five percent (`25%`) of the Members, the amendment to these Bylaws shall become effective by a two-thirds (`67%`) vote of Members. If notice of the character of the amendment proposed has been given in the notice of a meeting, these Bylaws may be altered or amended at any regular or special meeting of the Members by the affirmative vote of a simple majority of the Members present, or voting by mail or email, provided the Members so voting have received the exact wording of the amendments.

# Article XIII
## Merger, Consolidation, or Share or Equity Capital Exchange

### Section 1: Board and Member Approval of Merger, Consolidation, or Share or Equity Capital Exchange

Except as otherwise provided in Section 2 of this Article XIII, if the Cooperative is a party to a plan of merger, consolidation, or Share or equity capital exchange, such plan shall first be approved by a two-thirds vote (`67%`) of all the Members of the Board and then approved by a two-thirds (`67%`) vote of the Members present and voting in person or voting by mail or email, if voting by mail or email has been authorized by a majority of the Board. The provisions of Article VIII shall apply to any proceeds which may result from such merger, consolidation, or Share or equity capital exchange.

### Section 2: Merger of Cooperative Subsidiary

The Board may approve, in its discretion, by an affirmative two-thirds (`67%`) vote and without further membership approval or consent, a plan of merger of a subsidiary of the Cooperative into the Cooperative if the Cooperative owns one hundred percent (`100%`) of the voting Shares, memberships, or interests in the subsidiary and the Cooperative has the right to vote on behalf of the subsidiary; except, that if, as a result of the merger, the voting Shares, memberships or other interests of the members of the Cooperative would be materially altered, then the members shall have the right to vote on the plan of merger in a manner consistent with the provisions of Section 1 of this Article XIII.

# Article XIV
## Distribution of Bylaws

After adoption of these Bylaws or an amendment, a copy of these Bylaws or the amendment, as the case may be, shall be provided to each Member and other person qualified to Share in the Cooperative's Net Profits and to each person who later becomes a Member or person qualified to share in the Cooperative's Net Profits as shown on the books of record of the Cooperative.

# Article XV
## Right to Information; Confidentiality

The Cooperative shall maintain in record available at its principal office such information as is required by law. The Cooperative may maintain additional information in record, but shall not be required to make the same available unless required by law. Notwithstanding the immediately preceding sentence, and to the maximum allowable extent of the law, the Cooperative shall strive to protect the privacy rights of its Members and Member data from disclosure to law enforcement and other agencies requesting the same. The Cooperative strives to balance the privacy interest of its Members with the right to access information by the same. The Cooperative shall entertain requests for information by members and former members in accordance with applicable law. A member or former member making a valid request for information under this section and subject to applicable law, shall be solely responsible for paying or reimbursing the Cooperative for the reasonable costs associated with copying documents, including and limited to the cost of equipment, labor and materials.
Without limiting the generality of the foregoing, Members and former Members, shall at all times maintain in strict confidence and promise to not disclose any person or entity not otherwise entitled to receive such information any and all information received by or through the Cooperative, pertaining to the records of the Cooperative, its Members, and the operations, activities or transactions of the same. Each Member and former Member, whether receiving information consequent to a valid request for information under this section, or through its activities with or through the Cooperative, shall further ensure that any information transmitted or communicated to an attorney or other agent of such Member, shall be kept in confidence to the same degree and extent as the Member or former Member is or would be bound by this section. All membership information, fee schedules, financial information, correspondence and all other Cooperative documents and information furnished to the Member by the Cooperative will be kept in strict confidence.

Adopted:
__________________________________

Secretary:
__________________________________

To be incorporated:

# Article XVI
## Membership pipeline

We envision a single level of membership with a three step process for coming in. There are only member-owners, e.g. full members. For someone who wishes to join the process looks like this:

**1. Contractor membrane** - coop invites a contractor into relationship. Contractor signs The Contractor Agreement (general agreements, much like our service agreement). This document should outline what the contractor can expect from the coop (being respected, timely communication, etc) AND what is expected of the contractor (mandatory meetings, etc.). Also sign a Work Agreement, this is specific to each person and is very clear about what work they will be doing, this needs to be flexible and revisited often. They will keep the work agreement all the way through this process.

**2. Employee membrane** - after no less than 6 months (perhaps the board can "fast track" someone?) contractors can move to being employees. This means being on payroll. They sign an Employee contract and their work contract might be updated (it's being constantly reviewed and updated throughout their time with us).

**3. Membership membrane** - after no less than 6 months as an employee they are able to apply or be invited in as a member. This is where they sign the Membership Agreement and become a full member (I don't think there is a need for provisional membership, though it might be wise to make it a bit easy to kick out new members for 6 months?)

### Yearly Meeting Schedule [DOES THIS GET INCORPORATED INTO MEETINGS SECTION?]

- Spring Equinox: Annual In person member retreat (where we run the numbers)
- Summer Solstice: Meta meeting members & collaborators (talking about the meta structure of the coop)
- November 20: Intention Setting for the next year members & collaborators
- Monthly: members & invited collaborators Change Up meeting (info about change up)
Within those meetings we would like to fit all the required business. Each yearly meeting should have a theme and group the business based on that theme. e.g. spring meeting it about our how we did last year and how our intentions are panning out, summer is all about the meta view of the coop, and November is about setting intentions for the next year (and reflecting on the past year too). The Change Up meetings are about setting culture and protocol. These are where we want as much as possible for our agreements to be set and adjusted. I'm happy to go over this with you in detail and would love to hear your reaction and thoughts.

### Conflict Resolution [DOES THIS GET INCORPORATED INTO DISPUTE SECTION?]

We would like to work into the conflict resolution process something called Culture Committee. This is a group of people who agree to hear culture committee requests (kind of like the current bylaws, but we would like to build a cross business group, especially while we are small). The process is as follows:

1. First talk one on one with the person you're having an issue with,
2. next seek another person to help you mediate the situation, and
3. finally escalate to culture committee (CC). A web form can be submitted to the CC who will then put together a group to work it out.

I don't have all the details on how this will work, I'll think on it more, just want to flag it for now.

Rev 4/5/17_jwpc
Approved: June 7, 2017
Amended:

### we'd like to give all financial responsibilities to the treasurer, and all general management responsibilities to a new member role: cultural liaison. the president makes sure there is a cultural liaison who is appointed by the board and also takes over all general management tasks when the cultural liaison fails to fulfill their duties.

+katie@goodgoodwork.io, +drew@goodgoodwork.io
